/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
  description: "This DB includes all courses in system",
  courses: [
    {
      id: 1,
      courseCode: "FE_WEB_ANGULAR_101",
      courseName: "How to easily create a website with Angular",
      price: 750,
      discountPrice: 600,
      duration: "3h 56m",
      level: "Beginner",
      coverImage: "images/courses/course-angular.jpg",
      teacherName: "Morris Mccoy",
      teacherPhoto: "images/teacher/morris_mccoy.jpg",
      isPopular: false,
      isTrending: true
    },
    {
      id: 2,
      courseCode: "BE_WEB_PYTHON_301",
      courseName: "The Python Course: build web application",
      price: 1050,
      discountPrice: 900,
      duration: "4h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-python.jpg",
      teacherName: "Claire Robertson",
      teacherPhoto: "images/teacher/claire_robertson.jpg",
      isPopular: false,
      isTrending: true
    },
    {
      id: 5,
      courseCode: "FE_WEB_GRAPHQL_104",
      courseName: "GraphQL: introduction to graphQL for beginners",
      price: 850,
      discountPrice: 650,
      duration: "2h 15m",
      level: "Intermediate",
      coverImage: "images/courses/course-graphql.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: false
    },
    {
      id: 6,
      courseCode: "FE_WEB_JS_210",
      courseName: "Getting Started with JavaScript",
      price: 550,
      discountPrice: 300,
      duration: "3h 34m",
      level: "Beginner",
      coverImage: "images/courses/course-javascript.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: true
    },
    {
      id: 8,
      courseCode: "FE_WEB_CSS_111",
      courseName: "CSS: ultimate CSS course from beginner to advanced",
      price: 750,
      discountPrice: 600,
      duration: "3h 56m",
      level: "Beginner",
      coverImage: "images/courses/course-css.jpg",
      teacherName: "Juanita Bell",
      teacherPhoto: "images/teacher/juanita_bell.jpg",
      isPopular: true,
      isTrending: true
    },
    {
      id: 9,
      courseCode: "FE_WEB_WORDPRESS_111",
      courseName: "Complete Wordpress themes & plugins",
      price: 1050,
      discountPrice: 900,
      duration: "4h 30m",
      level: "Intermediate",
      coverImage: "images/courses/course-wordpress.jpg",
      teacherName: "Clevaio Simon",
      teacherPhoto: "images/teacher/clevaio_simon.jpg",
      isPopular: true,
      isTrending: false
    },
    {
      id: 10,
      courseCode: "FE_UIUX_COURSE_211",
      courseName: "Thinkful UX/UI Design Bootcamp",
      price: 950,
      discountPrice: 700,
      duration: "5h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-uiux.jpg",
      teacherName: "Juanita Bell",
      teacherPhoto: "images/teacher/juanita_bell.jpg",
      isPopular: false,
      isTrending: false
    },
    {
      id: 11,
      courseCode: "FE_WEB_REACRJS_210",
      courseName: "Front-End Web Development with ReactJs",
      price: 1100,
      discountPrice: 850,
      duration: "6h 20m",
      level: "Advanced",
      coverImage: "images/courses/course-reactjs.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: true
    },
    {
      id: 12,
      courseCode: "FE_WEB_BOOTSTRAP_101",
      courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
      price: 750,
      discountPrice: 600,
      duration: "3h 15m",
      level: "Intermediate",
      coverImage: "images/courses/course-bootstrap.png",
      teacherName: "Juanita Bell",
      teacherPhoto: "images/teacher/juanita_bell.jpg",
      isPopular: true,
      isTrending: false
    },
    {
      id: 14,
      courseCode: "FE_WEB_RUBYONRAILS_310",
      courseName: "The Complete Ruby on Rails Developer Course",
      price: 2050,
      discountPrice: 1450,
      duration: "8h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-rubyonrails.png",
      teacherName: "Claire Robertson",
      teacherPhoto: "images/teacher/claire_robertson.jpg",
      isPopular: false,
      isTrending: true
    }
  ]
}
var gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";
var gCourses = [];
var gPopular = [];
var gTrending = [];
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  callApiGetAllCourses();
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//gọi API để get all khóa học
function callApiGetAllCourses() {
  $.ajax({
    url: gBASE_URL + "/courses",
    type: "GET",
    dataType: "json",
    success: function (responseObj) {
      gCourses = responseObj;
      console.log("Khóa học");
      console.log(gCourses);
      for (var bI = 0; bI < gCourses.length; bI++) {
        if (gCourses[bI].isPopular === true) {
          gPopular.push(gCourses[bI]);
        }
        if (gCourses[bI].isTrending === true) {
          gTrending.push(gCourses[bI]);
        }
      }
      filterDataCoursesPopular(gPopular);
      filterDataCoursesTrending(gTrending);
    },
    error: function (xhr, status, error) {
      console.log(error);
    },
  });
}

//hàm lọc khóa học popular
function filterDataCoursesPopular(paramDataCourses) {
  console.log("Khóa học popular");
  console.log(paramDataCourses);
  loadDivToForm(paramDataCourses, 0, 4, "#div-card-most-popular");
}

//hàm lọc khóa học trending
function filterDataCoursesTrending(paramDataCourses) {
  console.log("Khóa học Trending");
  console.log(paramDataCourses);
  loadDivToForm(paramDataCourses, 0, 4, "#div-card-trending");
}

//hàm hiển thị lên form
function loadDivToForm(paramObject, paramStart, ParamEnd, paramIdDivCard) {
  // paramStart ParamEnd 
  for (var i = paramStart; i < ParamEnd; i++) {
    // tạo ra thẻ car html 
    var vCard = $(`<div class="col-sm-3">
    <div class="card-deck">
      <div class="card shadow card-hover">
        <img id="img-top-1" class="card-img-top" src="` + paramObject[i].coverImage + ` " alt="" style="width: 100%;">
      <div class="card-body">
       <h5 id="h1">` + paramObject[i].courseName + `</h5>
       <hr>
       <a> <i class="far fa-clock" aria-hidden="true"></i> &nbsp;   ` + paramObject[i].duration + `+ &nbsp; ` + paramObject[i].level + ` </a>
       <br>
       <p>Price: <span class="h5"><b>$` + paramObject[i].discountPrice + `</b></span> <s>$` + paramObject[i].price + `</s></p>
       <hr>
       <div class="row">
       <img class="col-sm-3 picture" src="` + paramObject[i].teacherPhoto + `"  alt="" />
       <div class="col-sm-9">
       <p class="text-muted">` + paramObject[i].teacherName + ` &nbsp; <i class="far fa-bookmark" aria-hidden="true"></i></p>
      </div>
    </div>
   </div>
  </div>`);
    $(vCard).appendTo(paramIdDivCard);
  }
}